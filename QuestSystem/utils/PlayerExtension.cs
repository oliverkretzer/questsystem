﻿using GTANetworkAPI;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestSystem.utils {
    public static partial class PlayerExtension {

        public static int getTeamID(this Client player) {
            if (player == null || !player.Exists || !hasTeamID(player)) return 0;
            return player.GetData("teamid");
        }

        public static void setTeamID(this Client player, int teamid) {
            if (player == null || !player.Exists) return;
            player.SetData("teamid", teamid);
        }

        public static bool hasTeamID(this Client player) {
            return player.HasData("teamid");
        }

    }
}
