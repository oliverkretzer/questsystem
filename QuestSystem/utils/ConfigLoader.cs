﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace QuestSystem.utils {
    public class ConfigLoader {

        public static Config config { get; set; }
        public static string connection { get; set; }

        public partial class Config {
            public string databaseHost { get; set; }
            public string databaseUser { get; set; }
            public string databasePass { get; set; }
            public string databaseDB { get; set; }
            public int databasePort { get; set; }
        }

        public static void fetchConfiguration() {
            string json = File.ReadAllText(@"config.json");
            Config cfg = JsonConvert.DeserializeObject<Config>(json);
            connection = $"server={cfg.databaseHost};user={cfg.databaseUser};password={cfg.databasePass};database={cfg.databaseDB};port={cfg.databasePort}";
            config = cfg;
        }

    }
}
