﻿using GTANetworkAPI;
using QuestSystem.handler;
using QuestSystem.utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestSystem.commands {
    class QuestCommand : Script {

        /*
         * Test Commands, zum Testen der DB / des Systems
         */

        [Command("quest", Alias = "q", GreedyArg = false)]
        public void Quest_Command(Client player) {
            try {
                QuestHandler.showAllQuests(player);
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }

        [Command("choosequest", Alias = "cq", GreedyArg = false)]
        public void ChooseQuest_Command(Client player, int questID) {
            try {
                QuestHandler.chooseQuest(player, questID);
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }

        [Command("finishquest", Alias = "fq", GreedyArg = false)]
        public void FinishQuest_Command(Client player, int questID) {
            try {
                QuestHandler.finishQuest(player, questID);
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
