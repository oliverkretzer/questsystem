﻿using Microsoft.EntityFrameworkCore;
using QuestSystem.database.dbmodels;
using QuestSystem.utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestSystem.database {
    public partial class DBLoader : DbContext{

        public DBLoader(DbContextOptions<DBLoader> options) : base(options) { }
        public DBLoader() { }

        public virtual DbSet<Quest> quest { get; set; }
        public virtual DbSet<QuestTeam> questTeam { get; set; }
        public virtual DbSet<QuestProgress> questProgress { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) {
            if (!optionsBuilder.IsConfigured) {
                optionsBuilder.UseMySql($"{ConfigLoader.connection}");
                optionsBuilder.EnableSensitiveDataLogging();
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) {
            modelBuilder.Entity<Quest>(entity => {
                entity.HasKey(e => e.id);
                entity.ToTable("quest", $"{ConfigLoader.config.databaseDB}");
                entity.HasIndex(e => e.id).HasName("id");

                entity.Property(e => e.id).HasColumnName("id");
                entity.Property(e => e.name).HasColumnName("name");
                entity.Property(e => e.description).HasColumnName("description");
                entity.Property(e => e.questPointsNeeded).HasColumnName("questPointsNeeded");
                entity.Property(e => e.questPoints).HasColumnName("questPoints");
                entity.Property(e => e.questGoal).HasColumnName("questGoal");
            });

            modelBuilder.Entity<QuestTeam>(entity => {
                entity.HasKey(e => e.id);
                entity.ToTable("questTeam", $"{ConfigLoader.config.databaseDB}");
                entity.HasIndex(e => e.id).HasName("id");

                entity.Property(e => e.id).HasColumnName("id");
                entity.Property(e => e.teamID).HasColumnName("teamID");
                entity.Property(e => e.questPoints).HasColumnName("questPoints");
                entity.Property(e => e.currentQuest).HasColumnName("currentQuest");
                entity.Property(e => e.currentQuestProgress).HasColumnName("currentQuestProgress");
            });

            modelBuilder.Entity<QuestProgress>(entity => {
                entity.HasKey(e => e.id);
                entity.ToTable("questProgress", $"{ConfigLoader.config.databaseDB}");
                entity.HasIndex(e => e.id).HasName("id");

                entity.Property(e => e.id).HasColumnName("id");
                entity.Property(e => e.teamID).HasColumnName("teamID");
                entity.Property(e => e.questID).HasColumnName("questID");
            });
        }
    }
}
