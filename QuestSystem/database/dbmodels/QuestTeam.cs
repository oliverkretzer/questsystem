﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace QuestSystem.database.dbmodels {
    public partial class QuestTeam {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public int teamID { get; set; }
        public int questPoints { get; set; }
        public int currentQuest { get; set; }
        public int currentQuestProgress { get; set; }
    }
}
