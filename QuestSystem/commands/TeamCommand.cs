﻿using GTANetworkAPI;
using QuestSystem.utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestSystem.commands {
    class TeamCommand : Script {

        /*
         *  Command um sich in ein Team zu setzen (zum Testen)
         */

        [Command("team", Alias = "t", GreedyArg = false)]
        public void Team_Command(Client player, int teamid = 0) {
            try {
                if (teamid == 0) {
                    player.SendChatMessage("Deine aktuelle TeamID: " + player.getTeamID());
                } else {
                    player.setTeamID(teamid);
                    player.SendChatMessage($"Deine TeamID wurde auf {teamid} gesetzt");
                }
            } catch (Exception e) {
                Console.WriteLine(e.ToString());
            }
        }
    }
}
