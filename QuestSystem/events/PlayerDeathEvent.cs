﻿using GTANetworkAPI;
using QuestSystem.handler;
using QuestSystem.utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestSystem.events {

    class PlayerDeathEvent : Script {

        [ServerEvent(Event.PlayerDeath)]
        public void OnPlayerDeath(Client player, Client killer, uint reason) {
            try {

                if (killer == null || !killer.Exists || killer.getTeamID() == 0) return;
                if (QuestHandler.sameQuest(killer, /* QUEST ID */ 1)) { // Hier wird geprüft, ob das Team aktuell die Quest mit id 1 macht.
                    QuestHandler.addQuestProgress(killer, 1); // 1 Kill
                    player.SendChatMessage(
                        "Dein Team hat nun " 
                        + QuestHandler.getCurrentTeamQuestProgress(player) + "/" + QuestHandler.getCurrentQuestGoal(player) 
                        + " Kills gemacht."
                    );
                }

            } catch(Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }

}
