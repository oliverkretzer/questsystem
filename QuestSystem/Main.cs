﻿using GTANetworkAPI;
using QuestSystem.commands;
using QuestSystem.database;
using QuestSystem.database.dbmanager;
using QuestSystem.handler;
using QuestSystem.utils;
using System;

namespace QuestSystem {
    public class Main : Script {

        [ServerEvent(Event.ResourceStart)]
        public void OnResourceStart() {
            try {
                // Config
                ConfigLoader.fetchConfiguration();

                // Database
                DBReader.readDatabase();

                // RAGEMP
                NAPI.Server.SetCommandErrorMessage($"~r~[FEHLER]~w~ Dieser Befehl wurde nicht gefunden.");

            } catch (Exception e) {
                Console.WriteLine("[Quest] Fehler beim starten des Systems!");
                Console.WriteLine(e.Message);
            }
        }

        [ServerEvent(Event.ResourceStop)]
        public void ServerEvent_ResourceStop() {
            Console.WriteLine("Server wird gestoppt.");
        }
    }
}
