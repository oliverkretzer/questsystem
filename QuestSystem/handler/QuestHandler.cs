﻿using GTANetworkAPI;
using QuestSystem.database.dbmanager;
using QuestSystem.utils;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestSystem.handler {
    public class QuestHandler {

        public static void showAllQuests(Client player) {
            if(player.getTeamID() == 0) {
                player.SendChatMessage("Du bist in keiner Fraktion!");
                return;
            }

            var questsNotQuests = QuestDBManager.getQuestNotDone(player.getTeamID());
            var questsDone = QuestDBManager.getQuestDone(player.getTeamID());

            player.SendChatMessage("Liste mit Quests:");
            questsNotQuests.ForEach(x => player.SendChatMessage("~r~" + x.name));
            questsDone.ForEach(x => player.SendChatMessage("~g~" + x.name));
        }

        public static void chooseQuest(Client player, int questID) {
            if (!QuestDBManager.existQuestTeam(player.getTeamID()) || !QuestDBManager.existQuest(questID)) {
                player.SendChatMessage("Dieses Team/Quest existiert nicht!");
                return;
            }
            if (QuestDBManager.hasQuestProgress(player.getTeamID(), questID)) {
                player.SendChatMessage("Dein Team hat diese Quest bereits gemacht!");
                return;
            }
            if (QuestDBManager.hasCurrentQuest(player.getTeamID())) {
                player.SendChatMessage("Dein Team hat bereits eine laufende Quest!");
                return;
            }
            if (QuestDBManager.getQuestTeam(player.getTeamID()).questPoints < QuestDBManager.getQuest(questID).questPointsNeeded) {
                player.SendChatMessage("Dein Team hat nicht genügend Quest Points um diese Quest zu starten!");
                return;
            }
            QuestDBManager.setCurrentQuest(player.getTeamID(), questID);
            player.SendChatMessage("Dein Team macht nun die Quest " + QuestDBManager.getQuest(questID).name);
        }

        public static void finishQuest(Client player, int questID) {
            if (!QuestDBManager.existQuest(questID) || !QuestDBManager.existQuestTeam(player.getTeamID())) {
                player.SendChatMessage("Dieses Team/Quest existiert nicht!");
                return;
            }
            var questTeam = QuestDBManager.getQuestTeam(player.getTeamID());
            if (questTeam.currentQuest != questID) {
                player.SendChatMessage("Dein Team macht diese Quest aktuell nicht!");
                return;
            }
            if (QuestDBManager.hasQuestProgress(questTeam.teamID, questID)) {
                player.SendChatMessage("Dein Team hat diese Quest bereits gemacht!");
                return;
            }
            if (!hasReachedQuestGoal(player)) {
                player.SendChatMessage("Dein Team hat die Quest noch nicht abgeschlossen!");
                return;
            }
            var quest = QuestDBManager.getQuest(questID);
            QuestDBManager.setCurrentQuest(questTeam.teamID, 0);
            QuestDBManager.setQuestProgress(questTeam.teamID, 0);
            QuestDBManager.addQuestPoints(questTeam.teamID, quest.questPoints);
            QuestDBManager.createQuestProgress(questTeam.teamID, questID);
            player.SendChatMessage(
                "Dein Team hat die Quest " + quest.name + "erfolgreich abgeschlossen und erhält " + quest.questPoints + " QuestPoints."
            );
        }

        public static void addQuestProgress(Client player, int amount) {
            QuestDBManager.addQuestProgress(player.getTeamID(), amount);
            if (hasReachedQuestGoal(player)) {
                finishQuest(player, QuestDBManager.getCurrentTeamQuest(player.getTeamID()));
            }
        }

        public static int getCurrentTeamQuestProgress(Client player) {
            if (player.getTeamID() == 0 || !QuestDBManager.hasCurrentQuest(player.getTeamID())) {
                return 0;
            }
            return QuestDBManager.getCurrentTeamQuestProgress(player.getTeamID());
        }

        public static int getCurrentQuestGoal(Client player) {
            if (player.getTeamID() == 0 || !QuestDBManager.hasCurrentQuest(player.getTeamID())) {
                return 0;
            }
            return QuestDBManager.getQuestGoal(QuestDBManager.getCurrentTeamQuest(player.getTeamID()));
        }

        public static bool sameQuest(Client player, int questID) {
            if (player.getTeamID() == 0 || !QuestDBManager.hasCurrentQuest(player.getTeamID())) {
                return false;
            }
            return QuestDBManager.getCurrentTeamQuest(player.getTeamID()) == questID;
        }

        private static bool hasReachedQuestGoal(Client player) {
            if (player.getTeamID() == 0 || !QuestDBManager.hasCurrentQuest(player.getTeamID())) {
                return false;
            }
            return QuestDBManager.hasReachedGoal(player.getTeamID(), QuestDBManager.getCurrentTeamQuest(player.getTeamID()));
        }
    }
}
