﻿using QuestSystem.database.dbmodels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace QuestSystem.database.dbmanager {
    public class QuestDBManager {

        public static List<Quest> quests;
        public static List<QuestTeam> questTeams;
        public static List<QuestProgress> questProgress;

        public QuestDBManager() {
            quests = new List<Quest>();
            questTeams = new List<QuestTeam>();
            questProgress = new List<QuestProgress>();
        }

        /*
         * INSERT INTO questTeam
         * ('teamID', 'questPoints', 'currentQuest', 'currentQuestProgress')
         * VALUES
         * (teamID, 0, 0, 0);
        */
        public static void createQuestTeam(int teamID) {
            try {
                if (teamID <= 0) return;

                QuestTeam questTeam = new QuestTeam {
                    teamID = teamID,
                    questPoints = 0,
                    currentQuest = 0,
                    currentQuestProgress = 0
                };

                questTeams.Add(questTeam);
                using (DBLoader db = new DBLoader()) {
                    db.questTeam.Add(questTeam);
                    db.SaveChanges();
                }
            } catch(Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        /*
        * INSERT INTO questProgress
        * ('teamID', 'questID')
        * VALUES
        * (teamID, questID);
        */
        public static void createQuestProgress(int teamID, int questID) {
            try {
                if (teamID <= 0 || questID <= 0) return;

                QuestProgress questprogress = new QuestProgress {
                    teamID = teamID,
                    questID = questID
                };

                questProgress.Add(questprogress);
                using (DBLoader db = new DBLoader()) {
                    db.questProgress.Add(questprogress);
                    db.SaveChanges();
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public static bool existQuest(int questID) {
            return quests.Exists(x => x.id == questID);
        }

        public static bool existQuestTeam(int teamID) {
            return questTeams.Exists(x => x.teamID == teamID);
        }

        public static bool hasQuestProgress(int teamID, int questID) {
            return questProgress.Exists(x => x.teamID == teamID && x.questID == questID);
        }

        public static bool hasCurrentQuest(int teamID) {
            return questTeams.Exists(x => x.teamID == teamID && x.currentQuest > 0);
        }

        public static bool hasReachedGoal(int teamID, int questID) {
            return getCurrentTeamQuestProgress(teamID) >= getQuestGoal(questID);
        }

        /*
         * SELECT *
         * FROM questTeam
         * WHERE teamID = teamID;
         */
        public static QuestTeam getQuestTeam(int teamID) {
            try {
                QuestTeam questTeam = questTeams.ToList().FirstOrDefault(x => x.teamID == teamID);
                if (questTeam != null) return questTeam;
            } catch(Exception e) {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        /*
         * SELECT *
         * FROM quest
         * WHERE questID = questID;
         */
        public static Quest getQuest(int questID) {
            try {
                Quest quest = quests.ToList().FirstOrDefault(x => x.id == questID);
                if (quest != null) return quest;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            return null;
        }

        public static List<Quest> getAllQuests() {
            return quests;
        }

        public static List<Quest> getQuestDone(int teamID) {
            var done = questProgress.Where(x => x != null && x.teamID == teamID).ToList();
            List<Quest> questList = new List<Quest>();
            foreach (var quest in done) {
                questList.Add(getAllQuests().Find(x => x.id == quest.questID));
            }
            return questList;
        }

        public static List<Quest> getQuestNotDone(int teamID) {
            var done = questProgress.Where(x => x != null && x.teamID == teamID).ToList();
            List<Quest> questList = new List<Quest>(getAllQuests());
            foreach (var quest in done) {
                questList.RemoveAll(x => x.id == quest.questID);
            }
            return questList;
        }

        public static int getQuestGoal(int questID) {
            try {
                Quest quest = quests.ToList().FirstOrDefault(x => x.id == questID);
                if (quest != null) return quest.questGoal;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            return 0;
        }

        public static int getCurrentTeamQuestProgress(int teamID) {
            try {
                QuestTeam questTeam = questTeams.ToList().FirstOrDefault(x => x.teamID == teamID);
                if (questTeam != null) return questTeam.currentQuestProgress;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            return 0;
        }

        public static int getCurrentTeamQuest(int teamID) {
            try {
                QuestTeam questTeam = questTeams.ToList().FirstOrDefault(x => x.teamID == teamID);
                if (questTeam != null) return questTeam.currentQuest;
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
            return 0;
        }

        /*
         * UPDATE questTeam
         * SET questPoints = (
         *      SELECT questPoints
         *      FROM questTeam
         *      WHERE teamID = teamID
         * ) + 10
         * WHERE teamID = teamID;
         */
        public static void addQuestPoints(int teamID, int questPoints) {
            try {
                var quest = questTeams.FirstOrDefault(x => x.teamID == teamID);
                if (quest == null) return;
                quest.questPoints += questPoints;
                using (DBLoader db = new DBLoader()) {
                    db.questTeam.Update(quest);
                    db.SaveChanges();
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public static void addQuestProgress(int teamID, int questProgress) {
            try {
                var quest = questTeams.FirstOrDefault(x => x.teamID == teamID);
                if (quest == null) return;
                quest.currentQuestProgress += questProgress;
                using (DBLoader db = new DBLoader()) {
                    db.questTeam.Update(quest);
                    db.SaveChanges();
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        /*
         * UPDATE questTeam
         * SET currentQuest = currentQuest
         * WHERE teamID = teamID;
         */
        public static void setCurrentQuest(int teamID, int currentQuest) {
            try {
                var quest = questTeams.FirstOrDefault(x => x.teamID == teamID);
                if (quest == null) return;
                quest.currentQuest = currentQuest;
                using (DBLoader db = new DBLoader()) {
                    db.questTeam.Update(quest);
                    db.SaveChanges();
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }

        public static void setQuestProgress(int teamID, int questProgress) {
            try {
                var quest = questTeams.FirstOrDefault(x => x.teamID == teamID);
                if (quest == null) return;
                quest.currentQuestProgress = questProgress;
                using (DBLoader db = new DBLoader()) {
                    db.questTeam.Update(quest);
                    db.SaveChanges();
                }
            } catch (Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }
}
