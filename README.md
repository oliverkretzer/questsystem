QuestSystem Funktionen:

Quest:
- ID (QuestID)
- Name
- Description (Anweisung was gemacht werden muss)
- questPointsNeeded (Anzahl der Quest-Punkte die man braucht um diese Quest zu starten | Wenn == 0 dann Starter Quest)
- questPoints (Anzahl der Quest-Punkte die man bekommt wenn man die Quest beendet)
- questGoal (z.B. Anzahl der Kills o.Ä. was benötigt wird um die Quest zu beenden)

QuestTeam:
- TeamID (Representiert eine Fraktion [z.B. Grove, Ballas, MG13, ...])
- questPoints (Anzahl der QuestPunkte die dieses Team erreich hat)
- currentQuest (Die Quest(QuestID) welche das QuestTeam aktuell macht | Wenn == 0 dann aktuell keine Quest die gemacht wird)
- currentQuestProgress (z.B. Anzahl der Kills o.Ä. welche Bereits erledigt wurden)

QuestProgress: (Welche Quest welches Team abgeschlossen hat)
- TeamID (QuestTeam)
- QuestID (Quest)
