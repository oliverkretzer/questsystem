﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace QuestSystem.database.dbmodels {
    public partial class Quest {

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        public string name { get; set; }
        public string description { get; set; }
        public int questPointsNeeded { get; set; }
        public int questPoints { get; set; }
        public int questGoal { get; set; }
    }
}
