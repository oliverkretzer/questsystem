﻿using QuestSystem.database.dbmodels;
using QuestSystem.database.dbmanager;
using System;
using System.Collections.Generic;
using System.Text;

namespace QuestSystem.database {
    public class DBReader {

        public static void readDatabase() {
            try {
                using (DBLoader db = new DBLoader()) {

                    QuestDBManager.quests = new List<Quest>(db.quest);
                    Console.WriteLine($"[Database - 'quest'] {QuestDBManager.quests.Count} Quests wurden geladen.");

                    QuestDBManager.questTeams = new List<QuestTeam>(db.questTeam);
                    Console.WriteLine($"[Database - 'questTeam'] {QuestDBManager.questTeams.Count} Quests-Teams wurden geladen.");

                    QuestDBManager.questProgress = new List<QuestProgress>(db.questProgress);
                    Console.WriteLine($"[Database - 'questProgress'] {QuestDBManager.questProgress.Count} QuestsProgress wurden geladen.");

                }
            }catch(Exception e) {
                Console.WriteLine(e.Message);
            }
        }
    }
}
